import re
import csv
import requests
from bs4 import BeautifulSoup

miit_url = "http://miit.gov.cn/"
cookie = ""
header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
    'Connection': 'keep-alive',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Cookie': cookie
}

def get_industry_all_url(soup, industry):
    html_text = soup.prettify().split("\n")
    pattern = ".*" + industry

    url_list = [miit_url + industry]

    for line in html_text:
        if re.match(pattern + "index", line, re.I) and not re.match(pattern + "index" + ".html", line, re.I) and re.match(".*href.*", line, re.I):
            searchObj = re.search(".*" + pattern + "(.*)\">", line, re.I)
            url_list.append(miit_url + industry + searchObj.group(1))
            # print(industry + searchObj.group(1))

    return url_list

def get_all_content_url(industry_all_url, title_text_pattern):

    headers = ["内容标题", "内容链接"]
    url_list = []

    with open('内容链接清单.csv', 'w', encoding='utf8', newline='') as f: 
        f_csv = csv.writer(f)

        f_csv.writerow(headers)

        for url in industry_all_url:

            request = requests.get(url, headers=header)
            request.encoding = "utf-8"
            content = request.text

            soup = BeautifulSoup(content, 'lxml')

            for i in soup.find_all(name='a',attrs={"href":re.compile(r'.*content.html$')}):
                title = i.get_text()
                content_url = i.get('href')
                if content_url.startswith("../../../"):
                    content_url = content_url.replace("../../../", miit_url)
                if not re.match("^(\\d*)-(\\d*)-(\\d*)$", title, re.I):            
                    if re.match(title_text_pattern, title, re.I):
                        # print((title, content_url))
                        f_csv.writerow((title, content_url))
                        url_list.append((title, content_url))

    return url_list


if __name__ == "__main__":

    # 通信业数据发布list endpoint
    communication = "n1146312/n1146904/n1648372/"

    # 获取全部发布的标题和URL
    # title_text_pattern = ".*"

    # 通信业主要指标完成情况
    title_text_pattern = "(\\d*)年(.*)月通信业主要指标完成情况（一）"
    # title_text_pattern = "(\\d*)年(.*)月通信业主要指标完成情况（二）"
    table_headers = ['年','月','指标名称','单位','本年本月止累计到达','比上年同期累计(±%)','本月']

    # 电话用户分省情况
    # title_text_pattern = "(\\d*)年(.*)月电话用户分省情况"
    # table_headers = ['年','月','区域名称','固定电话用户数','移动电话用户合计','移动电话4G用户']

    miit_comm_url = miit_url + communication

    request = requests.get(miit_comm_url, headers=header)
    request.encoding = "utf-8"
    miit_comm_stat_content = request.text

    soup = BeautifulSoup(miit_comm_stat_content, 'lxml')

    industry_all_report_url = set()
    # for i in soup.find_all(name='a',attrs={"href":re.compile(r'.*content.html$')}):
    #     if re.match("(\\d*)年(\\d*)月" + title_text_pattern, i.get_text(), re.I):
    #         # print(i.get_text())
    #         industry_all_report_url.add(i)

    industry_all_url = get_industry_all_url(soup, communication)

    all_content_url = get_all_content_url(industry_all_url, title_text_pattern)

    with open('result.csv', 'w', encoding='utf8', newline='') as f:

        f_csv = csv.writer(f)
        f_csv.writerow(table_headers)

        for (content_title, content_url) in all_content_url:

            print(content_title)
            print(content_url)

            request = requests.get(content_url, headers=header)
            request.encoding = "utf-8"
            content = request.text

            soup = BeautifulSoup(content, 'lxml')

            # con_title = soup.select('#con_title')[0].get_text()

            searchObj = re.search(title_text_pattern, content_title, re.I)

            year = searchObj.group(1)
            month = searchObj.group(2)

            # print(year)
            # print(month)

            ccontent = soup.select('.ccontent')

            # print(ccontent)

            for table_row in ccontent[0].find_all("tr"):
                row = [year, month]
                for table_data in table_row.find_all("td"):
                    row.append(re.sub('[\s+]', '', table_data.get_text()))

                if content_title.endswith("电话用户分省情况"):
                    print(row)
                    f_csv.writerow(row)
                else:
                    if len(row) >= 6 and not re.match(".*指.*标.*名.*称.*", row[2], re.I) and len(row[2]) > 0:
                        print(row)
                        f_csv.writerow(row)


